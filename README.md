# Basic Web Application

This project should easily help you create and deploy your new website using Python Flask and AngularJS and quickly deploy it to the cloud using AWS Elasticbeanstalk

# Get Started

## Requirements

- Git Client, (preferrably the cli, https://git-scm.com/downloads)
- Python (Python3+ https://www.python.org/downloads/)
- Python VirtualEnv (https://virtualenv.pypa.io/en/stable/installation/)
- NodeJS (https://nodejs.org/en/download/)
- Elasticbeanstalk CLI (http://docs.aws.amazon.com/elasticbeanstalk/latest/dg/eb-cli3-install.html)

__Each requirement has a guide on how to install them, and I won't be duplicating it here.__

To test each requirement
```
$ git --version
git version 2.13.1
$ python3 --version
Python 3.6.3
$ pip3 --version
pip 9.0.1 from /Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages (python 3.6)
$ virtualenv --version
15.1.0
$ eb --version
EB CLI 3.12.1 (Python 3.6.3)
$ node --version
v8.9.2
$ npm --version
5.5.1
$ npm install -g gulp
```
If any of this failed to respond their version go back and check for your installation.

## Getting Started

To get started you should have installed all the prerequisites above see [Requirements](##Requirements)

1. Clone this repository. 
```
$ git clone https://gitlab.com/jethroguce/web-app-stack.git web_app
$ cd web_app
```

2. Initialize development environment. 
```
$ virtualenv env -p python3
$ source env\bin\activate
$ pip install -r requirements.txt
$ npm install
$ gulp
```

3. Test Development Enviroment
```
$ python application.py
```
Open your browser at http://localhost:5000, you should see starter template

## Deploying Enviroment to Elasticbeanstalk

1. Initialize Elasticbeanstalk
```
$ eb init
```
Select your deployment region, provide details

2. Create Elasticbeanstalk deploy environment
```
$ eb create
```

provide environment name, i.e `webapp-dev` for development

provide cname, this will be your elasticbeanstalk url.

Select Load balancer type, select default

This will create and provision necessary AWS Resources.

3. Open via browser
```
$ eb open
```
This should open your default browser

## Updating Project

`git commit`, Any changes to the project source should be committed via git, this will create a snapshot version of your project

`eb deploy`, will deploy to elasticbeanstalk last committed version

## References:

http://flask.pocoo.org/docs/0.12/tutorial/
https://getbootstrap.com/docs/4.0/getting-started/introduction/
https://docs.angularjs.org/tutorial
http://docs.aws.amazon.com/elasticbeanstalk/latest/dg/create-deploy-python-apps.html
https://try.github.io/levels/1/challenges/1




var gulp = require('gulp');
var header = require('gulp-header');
var cleanCSS = require('gulp-clean-css');
var rename = require("gulp-rename");
var uglify = require('gulp-uglify');
var pkg = require('./package.json');

// Set the banner content
var banner = ['/*!\n',
    ' * Basic Web App - <%= pkg.title %> v<%= pkg.version %> (<%= pkg.homepage %>)\n',
    ' * Copyright 2017-' + (new Date()).getFullYear(), ' <%= pkg.author %>\n',
    ' * Licensed under <%= pkg.license.type %> (<%= pkg.license.url %>)\n',
    ' */\n',
    ''
].join('');

// Minify compiled CSS
gulp.task('minify-css', [], function() {
    return gulp.src('app/static/css/main.css')
        .pipe(cleanCSS({ compatibility: 'ie8' }))
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('app/static/css/'))
});


gulp.task('minify-js', [], function() {
    return gulp.src('app/static/js/app.js')
        .pipe(uglify())
        .pipe(header(banner, { pkg: pkg }))
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('app/static/js/'))
});

gulp.task('copy', function() {
    gulp.src(['node_modules/bootstrap/dist/**/*', '!**/npm.js', '!**/bootstrap-theme.*', '!**/*.map'])
        .pipe(gulp.dest('app/static/vendor/bootstrap'))

    gulp.src(['node_modules/angular/*', '!**/npm.js', '!**/*.gzip', '!**/*.map', '!**/*.json'])
        .pipe(gulp.dest('app/static/vendor/angular'))

    gulp.src(['node_modules/font-awesome/**', '!**/*.map', '!**/.npmignore', '!**/*.txt', '!**/*.md', '!**/*.json'])
        .pipe(gulp.dest('app/static/vendor/font-awesome'))
});

gulp.task('default', ['minify-css', 'minify-js', 'copy']);



// Dev task with browserSync
gulp.task('dev', ['minify-css', 'minify-js'], function() {
    gulp.watch('app/static/css/*.css', ['minify-css']);
    gulp.watch('app/static/js/*.js', ['minify-js']);
});

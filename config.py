import os

BIND_IP = '0.0.0.0'
BIND_PORT = 5000

try:
    DEBUG = True if str.upper(os.environ['DEBUG']) == 'TRUE' else False
except:
    DEBUG = False
